/*
 *  This software is free.
 */

package mrnomnom;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

/**
 * @author Andrew MacRae <liboff@gmail.com>
 *
 *
 */
public class Floater 
{
    public float x,y,vx,vy;
    public String content;
    public int width, height;
    public boolean drag,marked;
    private int deltaX, deltaY;
    
    public Floater(float tx,float ty,float tvx,float tvy,String s)
    {
        content = s;
        x = tx;
        y = ty;
        vx = tvx;
        vy = tvy;
        drag = false;
        deltaX = deltaY = 0;
    }
    
    public void draw(Graphics g)
    {
        Font f = new Font("Serif", Font.ITALIC, 42 );
        FontMetrics metrics = g.getFontMetrics(f);
        g.setFont(f);
        width = metrics.stringWidth(content)+4;
        height = metrics.getHeight()+4;
        if(drag)
        {
            g.setColor(Color.YELLOW);
            g.fillRect((int)(x-2),(int)(y-metrics.getHeight()),width,height);
            g.setColor(Color.black);
        }
        else
        {
            g.setColor(new Color(18,125,9));
            g.fillRect((int)(x-2),(int)(y-metrics.getHeight()),width,height);
            g.setColor(Color.black);
        }
        g.drawRect((int)(x-2),(int)(y-metrics.getHeight()),width,height);
        g.drawString(content, (int)x, (int)y);
    }
    public void move(double dvx, double dvy)
    {
        if(!drag)
        {
            vx+=dvx;
            vy+=dvy;
            x+=vx;
            y+=vy;
        }
    }
    
    public boolean isIn(int x0, int y0)
    {
        if(x0>x&&x0<x+width&&y0+height>y&&y0<y) return true;
        else return false;
    }
    public void setDrag(int mX, int mY)
    {
        drag = true;
        deltaX = mX - (int)x;
        deltaY = mY - (int)y;
    }
    public void handleDrag(int x0, int y0)
    {
        x = Math.min(Math.max(x0-deltaX,0),500-width);
        y = Math.min(Math.max(y0-deltaY,height),380);
        //System.out.println("(deltax,deltaY) = ("+deltaX+","+deltaY+")");
    }
    
    public char getCharValue()
    {
        return content.charAt(0);
    }
}