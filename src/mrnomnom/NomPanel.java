/**
 * Andrew MacRae liboff@gmail.com
 *
 * - Fix font metrics
 * - Fix timer to speed up letter production when only a few are on screen.
 * - Make Sure all necessary letters are on screen before random ones are made.
 * - Fix sour face
 */

package mrnomnom;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class NomPanel extends JPanel implements Runnable
{
    final int INTRO = 0, PLAYING = 1, WINNING = 2;
    int gameState;

    int tickCounter = 100;
    
    int size = 2;
    public Floater[] elems = new Floater[size];
    Thread fred;
    int W = 500;
    int H = 380;
    V2 flow = new V2(0, 0);
    int mouseX, mouseY;
    Image nomWait, nomChew1, nomChew2,nomWaveUp,nomWaveDown;

    Werd currWerd;
    String[] werdList;
    int chewTimer = 0;
    boolean chewing = false;
    boolean dragging = false;
    
    int chewTime = 120;
    int chews = 10;
    int mouthLX = 198;
    int mouthRX = 296;
    int mouthBY = 225;
    int mouthTY = 270;
    int addTimer = 300;
    int addCounter = addTimer;
    int maxLength = 6;
    int dragCounter = 0;

    public NomPanel()
    {
        init();
    }

    public void init()
    {   
        initWerdList();        
        currWerd = new Werd("BANANA");        
        resetWord();
        
        gameState = PLAYING;

        elems[0] = new Floater(120, 60, .4f, -.1f, Character.toString((char)(Math.random()*26+65)));
        elems[1] = new Floater(20, 160, -.4f, .8f, Character.toString((char)(Math.random()*26+65)));

        fred = new Thread(this);
        fred.start();

        try
        {
            nomWait = ImageIO.read(new File("Images/NomWaiting.png"));
            nomChew1 = ImageIO.read(new File("Images/NomChew1.png"));
            nomChew2 = ImageIO.read(new File("Images/NomChew2.png"));
            nomWaveUp = ImageIO.read(new File("Images/NomWaveUp.png"));
            nomWaveDown = ImageIO.read(new File("Images/NomWaveDown.png"));
        } catch (IOException e)
        {
            System.out.println("Error loading screen images");
            e.printStackTrace();
        }

    }

    public void run()
    {
        while (true)
        {
            if(gameState==PLAYING)
            {
                addCounter--;
                dragCounter++;
                if(addCounter<1)
                {
                    addFloater(getWerdChar());
                    int delta = Math.max(0, 72*(5-elems.length));
                    addCounter = addTimer-delta;
                }
                if (chewing)
                {
                    chewTimer--;
                    if (chewTimer < 1)
                    {
                        chewing = false;
                    }
                   // System.out.println("(ChewTimer,ChewTimer/25,ChewTimer/25%2) = (" + chewTimer + "," + chewTimer / 25 + "," + chewTimer / 25 % 2 + ")");
                }

                for (int i = 0; i < elems.length; i++)
                {
                    if (elems[i].x < 0 || elems[i].x + elems[i].width > W)
                    {
                        elems[i].vx *= -1;
                    }
                    if (elems[i].y < elems[i].height || elems[i].y > H)
                    {
                        elems[i].vy *= -1;
                    }
                }
            }
            else if (gameState == WINNING)
            {
                tickCounter--;
                if(tickCounter<0)
                {
                    gameState = PLAYING;
                    resetWord();
                }
            }
                
            try
            {
                Thread.sleep(20);
                repaint();
            } catch (InterruptedException e)
            {
                System.out.println(e.toString());
            }
        }
    }

    @Override
    public void paintComponent(Graphics g)
    {
        if(gameState == PLAYING)
        {
            if (chewing)
            {
                if ((chews * chewTimer / chewTime % 2 == 0))
                {
                    g.drawImage(nomChew2, 0, 0, 500, 380, this);
                } else
                {
                    g.drawImage(nomChew1, 0, 0, 500, 380, this);
                }
            } else if(dragging)
            {
                int t = dragCounter/2;
                if(t<16)
                {
                    if(((t/3)%4) == 0 || ((t/3)%4) == 2)
                    {
                        g.drawImage(nomWait, 0, 0, 500, 380, this);
                    }
                    else if ((t/3)%4 == 1)
                    {
                        g.drawImage(nomWaveUp, 0, 0, 496, 380, this);
                    }
                    else
                    {
                        g.drawImage(nomWaveDown, 0, 0, 496, 380, this);
                    }
                }
                else
                {
                    g.drawImage(nomWait, 0, 0, 500, 380, this);
                }
                if(t>32) dragCounter = 0;
            }
            else
            {
                g.drawImage(nomWait, 0, 0, 500, 380, this);
            }
            for (int i = 0; i < elems.length; i++)
            {
    //                elems[i].sendMouse(mouseX,mouseY);
                flow = fluidFlow(elems[i].x, elems[i].y);
                elems[i].move(flow.x, flow.y);
                elems[i].draw(g);
            }

            Font f = new Font("Serif", Font.ITALIC, 42 );
            FontMetrics metrics = g.getFontMetrics(f);
            g.setFont(f);
            int werdX0 = 80,werdY0 = 420;
            int charWidth = 30;
            int charHeight = 36;
            for(int k= 0;k<currWerd.getPos();k++)
            {
                //g.drawString(""+currWerd.getCharAtIndex(k),werdX0+k*metrics.charWidth(currWerd.getCharAtIndex(k)),werdY0);
                g.drawString(""+currWerd.getCharAtIndex(k),werdX0+k*charWidth,werdY0);
            }
            for(int k= currWerd.getPos();k<currWerd.getLength();k++)
            {
                g.fillRect(werdX0+k*charWidth, werdY0-charHeight+3, charWidth, charHeight);
            }

            g.drawImage(currWerd.getImage(), 600,20,250,250, this);
        }
        else if (gameState == WINNING)
        {
            g.setColor(new Color((tickCounter*1235)%255,(tickCounter*5321)%255,(tickCounter*4781273)%255));
            g.fillRect(0, 0, 900, 470);
        }
        
    }
    
    public void handleInput(char code, KeyEvent e)
    {
        switch (code)
        {
            case 'p': // Key Pressed
                if (e.getKeyCode() == KeyEvent.VK_UP)
                {
                }
                break;
            case 'r':  // Released
                break;
            case ('t'): // Typed
                char c = Character.toLowerCase(e.getKeyChar());
                switch (c)
                {
//                    case 'a':
//                        for(int j = 0;j < werds[0].getLength();j++)
//                        {
//                            System.out.print(" "+werds[0].getCharAtPos());
//                            werds[0].setPos(werds[0].getPos()+1);
//                        }
//                        break;
                        default:
                            addFloater(c);
                        break;
                }                
                break;
        }
    }

    public void handleMouse(char code, MouseEvent e)
    {
        switch (code)
        {
            case 'd':
                mouseX = e.getX();
                mouseY = e.getY();
                doDrag();
                break;
            case 'p':
                setDrag(e.getX(), e.getY());
                break;
            case 'r':
                releaseDrags(e.getX(), e.getY());
                break;
            case 'c':
                //resetWord();
                break;
        }
    }

    private void setDrag(int mX, int mY)
    {
        for (int i = 0; i < elems.length; i++)
        {
            if (elems[i].isIn(mX, mY))
            {
                elems[i].setDrag(mX, mY);
                dragging = true;
                dragCounter = 0;
            }
        }
    }

    private void releaseDrags(int mX, int mY)
    {
        for (int i = 0; i < elems.length; i++)
        {
            if (elems[i].drag)
            {
                elems[i].drag = false;
                if (mX > mouthLX && mX < mouthRX && mY > mouthBY && mY < mouthTY)
                {
                    chewTimer = chewTime;
                    chewing = true;
                    elems[i].marked = true;                  
                    if( (elems[i].content.toUpperCase()).equals((""+currWerd.getCharAtPos()).toUpperCase()))
                    {                        
                        currWerd.incPos();
                    }
                    System.out.println("currWerd.isComplete() returns "+currWerd.isComplete());
                    if(currWerd.isComplete())
                    {
                        System.out.println("We're done here!");
                        tickCounter = 100;
                        gameState = WINNING;
                    }
                }
            }
        }
        rebuildList();
        dragging = false;
    }

    private void rebuildList()
    {
        int deadPool = 0;
        for(int i = 0;i<elems.length;i++)
        {
            if(elems[i].marked) deadPool++;
        }
        Floater[] tmp = new Floater[elems.length-deadPool];
        int cnt = 0;
        int pnt = 0;
        while(cnt<tmp.length)
        {
            if(!elems[pnt].marked)
            {
                tmp[cnt] = elems[pnt];
                cnt++;
            }
            pnt++;
        }
        elems = tmp;
    }
    
    private void clearList()
    {
        elems = new Floater[0];
    }
            
    private void doDrag()
    {
        for (int i = 0; i < elems.length; i++)
        {
            if (elems[i].drag)
            {
                elems[i].handleDrag(mouseX, mouseY);
            }
        }
    }
    
    private void addFloater()
    {
        if(elems.length<maxLength)
        {
            String s = ""+(char)(Math.random()*26+65);
            Floater[] tmp = new Floater[elems.length+1];
            for(int i = 0;i<elems.length;i++)
            {
                tmp[i]=elems[i];
            }
            tmp[tmp.length-1] = new Floater(40+(int)(Math.random()*(W-80)),40+(int)(Math.random()*(W-80)), (float)Math.random(), (float)Math.random(), s);
            elems = tmp;
        }
        
    }

        private void addFloater(char c)
    {
        if(elems.length<maxLength)
        {
            String s = ""+c;
            Floater[] tmp = new Floater[elems.length+1];
            for(int i = 0;i<elems.length;i++)
            {
                tmp[i]=elems[i];
            }
            tmp[tmp.length-1] = new Floater(40+(int)(Math.random()*(W-80)),40+(int)(Math.random()*(W-80)), (float)Math.random(), (float)Math.random(), s);
            elems = tmp;
        }
        
    }

    public V2 fluidFlow(float x, float y)
    {
        double mag = Math.sqrt((x-W/2)*(x-W/2)+(y-H/2)*(y-H/2));
        V2 trap = new V2((x-W/2)/mag,(y-H/2)/mag);
        trap = trap.scale(-.2*Math.exp(mag*mag/(W*H)));
        V2 vec = new V2(0,0);
        if(x>0&&x<W&&y>0&&y<H)
        {
            vec = new V2((Math.sin(.01 * y) + Math.cos(.025 * x)), Math.sin(.01 * y) - Math.cos(.025 * x));
        }
        else
        {
            trap = trap.scale(10);
        }
        return vec.scale(.1).add(trap).scale(.01);
       //return(trap);
        
    }
    
    private void resetWord()
    {
        currWerd = new Werd(getWord());
        System.out.println(""+currWerd.getWerd());
    }
    
    private String getWord()
    {   
        String nextWerd = werdList[(int)(Math.random()*werdList.length)];
        System.out.println(nextWerd+" c.f. "+currWerd.getWerd());
        while(nextWerd.equals(currWerd.getWerd()))
        {
            nextWerd = werdList[(int)(Math.random()*werdList.length)];
            System.out.println(nextWerd+" c.f. "+currWerd.getWerd());
        }
        return nextWerd;
    }

// This is used to get ensure that the next char to spell the word is available.    
    private char getWerdChar()
    {
// Default case: if no new charcters are required, return a random capital letter.        
        char ret = (char)(Math.random()*26+65);
// Generate char array corresponding to current word.
        char[] werdChars = currWerd.getWerd().toCharArray();
// Start at the next unknown letter, search buffer for all remaining letters.
        for(int i = currWerd.getPos();i<werdChars.length;i++)
        {
            if(!isInList(werdChars[i])) return werdChars[i];
        }
        return ret;
    }
// Is the char in question in the Floater buffer?    
    private boolean isInList(char c)
    {
        for(int i = 0;i<elems.length;i++)
        {
            if(c==elems[i].getCharValue()) return true;            
        }                 
        return false;
    }
    
    private boolean initWerdList()
    {
// First, count the number of lines in the word-list file and initialize the array
        String fName = "werdlist.txt";
        int nLines = -1;
        try
        {
            nLines = countLines(fName);
        }
        catch (IOException ex)
        {
            Logger.getLogger(NomPanel.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        System.out.println("Found "+nLines+" lines. Loading arrays ...");
        werdList = new String[nLines];
        if(nLines<1) return false;
// Next populate the array
        try(BufferedReader br = new BufferedReader(new FileReader(fName))) 
        {
            int cnt = 0;
            String line = br.readLine();
            
            while (line != null) 
            {
                werdList[cnt] = line;
                cnt++;                
                line = br.readLine();
            }
        }
        catch(IOException e)
        {
            Logger.getLogger(NomPanel.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }        
        
        for(int i = 0;i<werdList.length;i++)
        {
            System.out.println(""+werdList[i]);
        }
        return true;
    }
    
    public int countLines(String filename) throws IOException
    {
        InputStream is = new BufferedInputStream(new FileInputStream(filename));
        try
        {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean endsWithoutNewLine = false;
            while ((readChars = is.read(c)) != -1)
            {
                for (int i = 0; i < readChars; ++i)
                {
                    if (c[i] == '\n')
                    {
                        ++count;
                    }
                }
                endsWithoutNewLine = (c[readChars - 1] != '\n');
            }
            if (endsWithoutNewLine)
            {
                ++count;
            }
            return count;
        } finally
        {
            is.close();
        }
    }
}
