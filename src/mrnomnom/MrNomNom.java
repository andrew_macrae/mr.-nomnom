/*
 *  This software is free.
 */
package mrnomnom;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 * @author amacrae
 */
public class MrNomNom extends JFrame implements KeyListener, MouseListener, MouseMotionListener
{

    NomPanel nPanel = new NomPanel();
    public static void main(String[] args)
    {
        MrNomNom mf = new MrNomNom();
        mf.setVisible(true);
        mf.init();        
    }

    public MrNomNom()
    {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900, 470);
        setTitle("Mr. Nom Nom");
        Container content = getContentPane();

        content.add(nPanel, BorderLayout.CENTER);

    }
    public void keyPressed(KeyEvent k)
    {
        nPanel.handleInput('p',k);
    }

    public void keyReleased(KeyEvent k)
    {
        nPanel.handleInput('r',k);
    }

    public void keyTyped(KeyEvent k)
    {
        nPanel.handleInput('t',k);
    }
    public void mousePressed(MouseEvent e)
    {
        nPanel.handleMouse('p', e);
    }
    public void mouseReleased(MouseEvent e)
    {
        nPanel.handleMouse('r', e);
    }
    public void mouseClicked(MouseEvent e)
    {
        nPanel.handleMouse('c', e);
    }
    public void mouseEntered(MouseEvent e)
    {        
    }
    public void mouseExited(MouseEvent e)
    {
    }
    public void mouseMoved(MouseEvent e)
    {
        nPanel.handleMouse('m', e);
    }
    public void mouseDragged(MouseEvent e)
    {
        nPanel.handleMouse('d', e);
    }
    
    public void init()
    {
        nPanel.setFocusable(true);
        nPanel.addKeyListener(this);
        nPanel.addMouseListener(this);
        nPanel.addMouseMotionListener(this);
    }
}