/*
 *  This software is free.
 */

package mrnomnom;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * @author Andrew MacRae <liboff@gmail.com>
 *
 *
 */
public class Werd 
{
    private char[] werd;
    private int len;
    private int pos;
    private boolean completed;
    private Image image;
    
    public Werd(String s)
    {
        werd = s.toUpperCase().toCharArray();
        len = werd.length;
        pos = 0;
        completed = false;
        try
        {
            image = ImageIO.read(new File("Images/"+s+".jpg"));
        } 
        catch (IOException e)
        {
            System.out.println("Error loading screen images!!!");
            e.printStackTrace();
        }

    }
    
    public char getCharAtPos()
    {
        if(pos<len) 
        {
            return werd[pos];
        }
        else
        {   // Returnnull charachter if error.
            System.out.println("Error: attempted to access Character beyond word!");
            return '\0';
        }
    }
    public char getCharAtIndex(int k)
    {
        if(k<len) 
        {
            return werd[k];
        }
        else
        {   // Returnnull charachter if error.
            System.out.println("Error: attempted to access Character beyond word!");
            return '\0';
        }
    }
    
    public boolean setPos(int pos2)
    {
        if(pos2>=0&&pos2<=len)
        {
            pos = pos2;
            return true;
        }
        else
        {
            System.out.println("Could not increment position:\nasked to go to "+pos2+" but len = "+len);
            return false;
        }        
    }
    
    public boolean incPos()
    {        
        if(pos<=len)
        {
            pos++;
            if(pos==len) completed = true;
            return true;
        }
        else return false;
    }
    
    public int getPos()
    {
        return pos;
    }
    
    public int getLength()
    {
        return len;
    }
    
    public boolean isComplete()
    {
        return completed;
    }
    public String getWerd()
    {
        return String.valueOf(werd);
    }
    
    public Image getImage()
    {
        return image;
    }
}