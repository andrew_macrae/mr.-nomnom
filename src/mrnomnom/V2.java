/*
 *  This software is free.
 */

package mrnomnom;

/* *
 * Andrew MacRae liboff@gmail.com
 * */
public class V2 
{
    public double x;
    public double y;
    
    public V2(double u, double v)
    {
        x = u;
        y = v;
    }
    
    public double mag()
    {
        return Math.sqrt(x*x+y*y);
    }
    public V2 unit()
    {
        double m = mag();
        if(m==0)
        {
            return new V2(0,0);
        }
        else
        {
            return new V2(x/m,y/m);
        }
    }
    public V2 perp() // return unit unit vector perpendicular to V, oriented counterclockwise.
    {
        double m = mag();
        if(m==0)
        {
            return new V2(0,0);
        }
        else
        {
            return new V2(y/m,-x/m);
        }
    }
    
    public V2 scale(double d)
    {
        return new V2(x*d,y*d);
    }
    
    public V2 add(V2 v)
    {
        return new V2(x+v.x,y+v.y);
    }
    public V2 subt(V2 v) // Let's not be to RISCy. returns (this vector) - v
    {
        return new V2(x-v.x,y-v.y);
    }
    public double dot(V2 v)
    {
        return (x*v.x + y*v.y);
    }
    public String toString()
    {
        return "("+x+","+y+")";
    }
    public void addX(double d) // to avoid making new V2s left right and center.
    {
        x+=d;
    }    
    public void addY(double d)
    {
        y+=d;
    }
}